use std::fs;

fn main() {
  let mut puzzle = fs::read_to_string("puzzle.txt").expect("Something went wrong reading the file");
  puzzle.pop();
  let c: Vec<&str> = puzzle.split("\n").collect();
  let v: Vec<f64> = c.into_iter().map(|x| x.parse().unwrap()).collect();

  // Part 1
  let fuel = part_one(&v);
  println!("Part 1: Fuel is {}", fuel);

  // Part 2
  let fuel = part_two(&v);
  println!("Part 2: Fuel is {}", fuel);
}

fn part_one(v: &Vec<f64>) -> f64 {
  // Part 1
  // take its mass, divide by three, round down, and subtract 2
  let mut fuel: f64 = 0.0;
  for mass in 0..v.len() {
    fuel += ((v[mass] / 3.0).floor()) - 2.0;
  }
  fuel
}

fn part_two(v: &Vec<f64>) -> f64 {
  // Part 2
  // take its mass, divide by three, round down, and subtract 2
  // but do that for each mass until it is negative or zero
  let mut v2 = Vec::new();

  for m in 0..v.len() {
    calculate_fuel(v[m], &mut v2);
  }

  let fuel: f64 = v2.iter().sum();
  fuel
}

fn calculate_fuel(m: f64, v: &mut Vec<f64>) {
  let fuel: f64 = (m / 3.0).floor() - 2.0;
  if fuel > 0.0 {
    v.push(fuel);
    calculate_fuel(fuel, v);
  }
}
