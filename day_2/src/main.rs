use std::fs;

fn main() {
  let mut puzzle = fs::read_to_string("puzzle.txt").expect("Something went wrong reading the file");
  puzzle.pop();
  let c: Vec<&str> = puzzle.split(",").collect();
  let mut v: Vec<usize> = c.into_iter().map(|x| x.parse().unwrap()).collect();

  let mut v2 = v.clone();

  let part_1_ans = part_one(&mut v, 12, 2);
  println!("Part 1 result: {}", part_1_ans);

  let part_2_ans: isize = part_two(&mut v2);
  println!("Part 2 result: {}", part_2_ans);
}

// Part One
fn part_one(v: &mut Vec<usize>, noun: usize, verb: usize) -> usize {
  let mut op_code: usize;
  let mut input_1: usize;
  let mut input_2: usize;
  let mut position: usize;

  // To start, restore computer to the 1202 program alarm by replacing position 1 with the value
  // 12 and replace position 2 with the value 2
  v[1] = noun;
  v[2] = verb;

  for i in (0..v.len()).step_by(4) {
    if v[i] == 99 {
      return v[0];
    }
    op_code = v[i];
    input_1 = v[i + 1];
    input_2 = v[i + 2];
    position = v[i + 3];

    if op_code == 1 {
      let val = v[input_1] + v[input_2];
      v[position] = val;
    } else if op_code == 2 {
      let val = v[input_1] * v[input_2];
      v[position] = val;
    } else if op_code == 99 {
      println!("caught op_code 99. returning ...");
      return v[0];
    } else {
      println!("Bad op code. Op code is {}", op_code);
    }
  }
  v[0]
}

// Part Two
fn part_two(v2: &mut Vec<usize>) -> isize {
  let output = 19690720; // date of moon landing

  // To start, restore computer to the the 0-99 for the noun and 0-99 for the verb
  // When v[0] is 19690720 then multiply the noun by 100 and add the verb.
  for a in 0..100 {
    for b in 0..100 {
      let mut v = v2.clone();

      let ans = part_one(&mut v, a, b);
      if ans == output {
        return ((100 * a) + b) as isize;
      }
    }
  }
  -1 // error
}
