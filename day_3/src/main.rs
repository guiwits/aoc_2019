use std::collections::{HashMap, HashSet};
use std::fs;

#[derive(Debug, Clone)]
struct Point {
  x: i64,
  y: i64,
}

impl Point {
  fn new(x: i64, y: i64) -> Point {
    Point { x: x, y: y }
  }

  fn origin() -> Point {
    Point { x: 0, y: 0 }
  }
}

fn main() {
  let mut puzzle = fs::read_to_string("test_1.in").expect("Something went wrong reading the file");
  puzzle.pop();

  // split puzzle into two elments, wire one and wire two
  // then collect the elements of each wire into a vector of strings
  let wires: Vec<&str> = puzzle.split("\n").collect();
  let wire_one: Vec<&str> = wires[0].split(',').collect();
  let wire_two: Vec<&str> = wires[1].split(',').collect();

  let ans_one = part_one(&wire_one, &wire_two);
  let ans_two = part_two(&wire_one, &wire_two);

  println!("answer one = {}", ans_one);
  println!("answer two = {}", ans_two);
}

fn part_one(wire_one: &Vec<&str>, wire_two: &Vec<&str>) -> i64 {
  // Manhatten Distance:
  // The distance between (p1, p2) and (q1, q2) is |p1 − q1| + |p2 − q2|

  // Create HashMap to store direction and distance
  let mut direction_and_distance: HashMap<char, i64> = HashMap::new();
  for i in 0..wire_one.len() {
    let direction: char = wire_one[i].chars().next().unwrap();
    let distance: i64 = wire_one[i][1..].parse::<i64>().unwrap();
    match direction {
      'U' => println!("UP!"),
      'D' => println!("DOWN!"),
      'L' => println!("LEFT!"),
      'R' => println!("RIGHT!"),
      _ => println!("Bad Direction"),
    }
  }
  0
}

fn part_two(wire_one: &Vec<&str>, wire_two: &Vec<&str>) -> i64 {
  println!("Wire 1 length = {:?}", wire_one.len());
  println!("Wire 2 length = {:?}", wire_two.len());
  0
}
