fn main() {

  //let start: u64 = 273025;
  //let end:   u64 = 767253;
  let start: u64 = 240298;
  let end:   u64 = 784956;

  let (total_1, total_2) = single_threaded (start, end);

  println!("total 1 {} total 2: {}", total_1, total_2);
}

fn single_threaded(start: u64, end: u64) -> (u64, u64) {
    let mut a: u64   = 0;
    let mut b: u64   = 0;

    for mut i in start..end + 1 {
        let (p1, p2) = check_number(&mut i);
        if p1 {
            a = a + 1;
        }
        if p2 {
            b = b + 1;
        }
    }

    (a, b)
}

fn check_number(i: &mut u64) -> (bool, bool) {
  let mut sequence_length: u64 = 0;
  let mut last: u64 = 10;
  let mut part_one: bool = false;
  let mut part_two: bool = false;

  while i > &mut 0 {
    let val: u64 = *i % 10;
    *i = *i / 10;

    if val > last {
        return (false, false);
    } else if val == last {
        part_one = true;
        sequence_length = sequence_length + 1;
    } else {
        if sequence_length == 1 {
          part_two = true
        }
        sequence_length = 0;
    }
    last = val;
  }

  if sequence_length == 1 {
    (part_one, false)
  } else {
    (part_one, part_two) 
  }
}

fn is_valie (i: u64) -> bool {
    if !has_same_adjacent_values(i) {
        return false;
    }

    if !increasing_left_to_right(i) {
        return false;
    }

    true
}

fn has_same_adjacent_values(num: u64) -> bool {
  false
}

fn increasing_left_to_right(num: u64) -> bool {
  false
}
















