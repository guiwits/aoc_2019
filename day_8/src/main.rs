use std::fs;
use std::str;

fn main() {
  let pix_wide: usize = 25;
  let pix_tall: usize = 6;
  let mut input = fs::read_to_string("day_8.in").expect("Something went wrong reading the file");
  input.pop();
  let lines: Vec<&str> = sub_strings(&input, pix_wide * pix_tall); // 100
  let mut lowest: usize = 100;
  let mut idx: usize = 0;

  // Our input is 25 pixels wide and 6 pixels tall.
  // So each layer is 150 characters.
  // Find the layer that contains the fewest 0 digits.
  // On that layer, what is the number of 1 digits multiplied by the number of 2 digits?
  for i in 0..lines.len() {
    // split each line by char and then put the number of zeros into an map (line, #0)
    let char_vec: Vec<char> = lines[i].chars().collect();
    let mut count = 0;
    for c in char_vec {
      if c == '0' {
        count = count + 1;
      }
    }
    if lowest > count {
      lowest = count;
      idx = i;
    }
  }
  println!("index {}, count {}", idx, lowest);

  let mut number_ones: usize = 0;
  let mut number_twos: usize = 0;
  let char_vec: Vec<char> = lines[idx].chars().collect();
  for c in char_vec {
    if c == '1' {
      number_ones = number_ones + 1;
    } else if c == '2' {
      number_twos = number_twos + 1;
    }
  }
  println!("ones * twos = {}", number_ones * number_twos);
}

fn sub_strings(string: &str, sub_len: usize) -> Vec<&str> {
  let mut subs = Vec::with_capacity(string.len() / sub_len);
  let mut iter = string.chars();
  let mut pos = 0;

  while pos < string.len() {
    let mut len = 0;
    for ch in iter.by_ref().take(sub_len) {
      len += ch.len_utf8();
    }
    subs.push(&string[pos..pos + len]);
    pos += len;
  }
  subs
}
